package gwt.client;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.TreeViewModel;
import gwt.client.source.*;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Xrup on 19.12.2014.
 */
public class CustomTreeModel implements TreeViewModel, Serializable
{
    List<Country> countries;

    public CustomTreeModel(){

    }

    public CustomTreeModel(List<Country> countries){
        this.countries = countries;
    }


    private <T extends Area> NodeInfo<?> getNI(List<T> data){
        ListDataProvider<T> dataProvider = new ListDataProvider<T>(data);

        Cell<T> cell = new AbstractCell() {
            @Override
            public void render(Context context, Object value, SafeHtmlBuilder sb) {
                if (value != null) {
                    sb.appendHtmlConstant("    ");
                    sb.appendEscaped(((T)value).getName());

                }
            }
        };

        return new DefaultNodeInfo<T>(dataProvider, cell);
    }

    @Override
    public <T> NodeInfo<?> getNodeInfo(T value) {
        if (value == null) {
            return getNI(countries);
        } else if (value instanceof Country) {
            return getNI(( new LinkedList<City>(((Country) value).getAreas())));
        } else if (value instanceof City) {
            return getNI(( new LinkedList<Street>(((City) value).getAreas())));
        } else if(value instanceof Street){
            return getNI(( new LinkedList<Building>(((Street) value).getAreas())));
        } else if(value instanceof Building){
            return getNI(( new LinkedList<Room>(((Building) value).getAreas())));
        }
        return null;
    }

    @Override
    public boolean isLeaf(Object value) {
        return false;
    }
}
