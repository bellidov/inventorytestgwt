package gwt.client.source;


import gwt.client.source.devices.Device;

import java.io.Serializable;

public class Room extends Area<Server> implements Serializable {


    public Room(String name) {
        super(name, new Server[0]);
    }

    public Room(String name, Server[] device){
        super(name, device);
    }


    public Room() {
    }
}