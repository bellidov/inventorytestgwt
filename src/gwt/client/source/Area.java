package gwt.client.source;

/**
 * Created by Xrup on 15.12.2014.
 */
import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;


public abstract class Area<A extends Area<?>> implements Serializable {

    protected String name;
    protected Area<?> parent;
    protected Set<A> areas;

    public Area(){

    }
    protected Area(String name, A[] areas) {
        this.name = name;
        this.areas = new TreeSet<A>(new AreaComparator<A> ());
        for(A area : areas) {
            area.parent = this;
            this.areas.add(area);
        }
    }

    public String getName() {
        return name;
    }

    public Set<A> getAreas() {
        return areas;
    }

    public Area<?> getParent() {
        return parent;
    }

    public String toString(){
        return name;
    }


}
