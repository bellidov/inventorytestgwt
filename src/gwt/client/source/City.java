package gwt.client.source;

import java.io.Serializable;

public class City extends Area<Street> implements Serializable {

    public City(String name, Street[] streets) {
        super(name, streets);
    }

    public City() {
    }
}
