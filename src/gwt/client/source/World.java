package gwt.client.source;


import java.io.Serializable;

public class World extends Area<Country> implements Serializable {

    public World(String name, Country[] countries) {

        super(name, countries);
    }

    public World() {

        this("Earth", new Country[]{
                new Country("Morocco", new City[] {
                        new City("Casablanca", new Street[]{
                                new Street("2 mars", new Building[]{new Building("1a", new Room[]{new Room("1"), new Room("2")}), new Building("2b", new Room[]{new Room("4"), new Room("5")}), new Building("3a", new Room[]{new Room("1"), new Room("2")})}),
                                new Street("Hay Hassani", new Building[]{new Building("12a", new Room[]{new Room("1"), new Room("2")}), new Building("45g", new Room[]{new Room("1"), new Room("2")})}),
                                new Street("Sbata", new Building[]{new Building("33b", new Room[]{new Room("1"), new Room("2")}), new Building("45g", new Room[]{new Room("1"), new Room("2")})})}),
                        new City("Fes", new Street[]{
                                new Street("Kassem", new Building[]{new Building("9a", new Room[]{new Room("1"), new Room("2")}), new Building("4g", new Room[]{new Room("1"), new Room("2")})}),
                                new Street("Ibrahim", new Building[]{new Building("32a", new Room[]{new Room("1"), new Room("2")}), new Building("45g", new Room[]{new Room("1"), new Room("2")})})}),
                        new City("Marrakech", new Street[]{
                                new Street("Fanaa", new Building[]{new Building("80a", new Room[]{new Room("1"), new Room("2")}), new Building("45g", new Room[]{new Room("1"), new Room("2")})}),
                                new Street("Ibn elhaitham", new Building[]{new Building("32a", new Room[]{new Room("1"), new Room("2")}), new Building("45g", new Room[]{new Room("1"), new Room("2")})})}),
                }),
                new Country("Russia", new City[] {
                        new City("Nijny Novogord", new Street[]{
                                new Street("Gagarina", new Building[]{new Building("90a", new Room[]{new Room("1"), new Room("2")}), new Building("10g", new Room[]{new Room("1"), new Room("2")})}),
                                new Street("Lenina", new Building[]{new Building("58a", new Room[]{new Room("1"), new Room("2")}), new Building("40h", new Room[]{new Room("1"), new Room("2")})}),
                                new Street("Geroev",new Building[]{new Building("50", new Room[]{new Room("1"), new Room("2")}), new Building("4g", new Room[]{new Room("1"), new Room("2")})})})
                }),

        });

    }


}
