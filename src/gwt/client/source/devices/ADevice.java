package gwt.client.source.devices;

import gwt.client.source.Area;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Xrup on 21.12.2014.
 */
public abstract class ADevice extends Area<Area<?>> implements Device, Serializable {

    private List<Device> components;
    private String name;

    public ADevice(){

    }

    public ADevice(String name){
        components = new LinkedList<Device>();
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Device> getComponents() {
        return components;
    }

    @Override
    public void addComponent(Device device) {
        components.add(device);
    }
}
