package gwt.client.source.devices;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Xrup on 21.12.2014.
 */
public interface Device extends Serializable{
    String getName();
    List<Device> getComponents();
    void addComponent(Device device);
}
