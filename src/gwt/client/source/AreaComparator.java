package gwt.client.source;

import java.io.Serializable;
import java.util.Comparator;
import java.util.NavigableMap;

public class AreaComparator<A extends Area<?>> implements Comparator<A>, Serializable{

    @Override
    public int compare(A o1, A o2) {
        return o1.getName().compareTo(o2.getName());
    }

}
