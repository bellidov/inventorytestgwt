package gwt.client.source;


import java.io.Serializable;

public class Street extends Area<Building> implements Serializable{

    public Street(String name, Building[] buildings) {
        super(name, buildings);
    }

    public Street() {
    }
}