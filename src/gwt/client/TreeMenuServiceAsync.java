package gwt.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import gwt.client.source.World;

/**
 * Created by Xrup on 19.12.2014.
 */
public interface TreeMenuServiceAsync {
    void getData(AsyncCallback<World> asyncCallback);
}
