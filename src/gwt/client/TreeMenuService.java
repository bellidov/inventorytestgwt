package gwt.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;
import gwt.client.source.World;

/**
 * Created by Xrup on 19.12.2014.
 */
@RemoteServiceRelativePath("TreeMenuService")
public interface TreeMenuService extends RemoteService {
    World getData();
    /**
     * Utility/Convenience class.
     * Use TreeMenuService.App.getInstance() to access static instance of TreeMenuServiceAsync
     */
    public static class App {
        private static final TreeMenuServiceAsync ourInstance = (TreeMenuServiceAsync) GWT.create(TreeMenuService.class);

        public static TreeMenuServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
