package gwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.cellview.client.CellTree;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TreeNode;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.TreeViewModel;
import gwt.client.source.*;

import java.util.LinkedList;

/**
 * Created by Xrup on 19.12.2014.
 */
public class TreeMenu implements EntryPoint {

    private World world;
    private Panel mainPanel;
    Tree t;

    public void onModuleLoad() {

        TreeMenuService.App.getInstance().getData(new AsyncCallback<World>() {
            @Override
            public void onFailure(Throwable caught) {

            }

            @Override
            public void onSuccess(World result) {
                world = result;

                TreeItem root = new TreeItem();
                root.setText(world.getName());

                for(Country country : world.getAreas()){
                    TreeItem countryItem = new TreeItem();
                    countryItem.setText(country.getName());
                    root.addItem(countryItem);
                    for(City city : country.getAreas()){
                        TreeItem cityItem = new TreeItem();
                        cityItem.setText(city.getName());
                        countryItem.addItem(cityItem);
                        for(Street street : city.getAreas()){
                            TreeItem streetItem = new TreeItem();
                            streetItem.setText(street.getName());
                            cityItem.addItem(streetItem);
                            for(Building building : street.getAreas()){
                                TreeItem buildingItem = new TreeItem();
                                buildingItem.setText(building.getName());
                                streetItem.addItem(buildingItem);
                                for(Room room :  building.getAreas()){
                                    TreeItem roomItem = new TreeItem();
                                    roomItem.setText(room.getName());
                                    buildingItem.addItem(roomItem);

                                    for(Server server : room.getAreas()) {
                                        if(server != null){
                                            final Label text = new Label(server.getName());
                                            text.addMouseOverHandler(new MouseOverHandler() {
                                                @Override
                                                public void onMouseOver(MouseOverEvent event) {
                                                    text.addStyleName("labelOn");
                                                }
                                            });
                                            TreeItem serverItem = new TreeItem(text);

                                         //   serverItem.setText(text.getText());
                                            roomItem.addItem(serverItem);
                                        }
                                    }
                                }
                            }
                        }
                    }

                }


                t = new Tree();
                t.addItem(root);
                /*
                CustomTreeModel model = new CustomTreeModel(new LinkedList<Country>(world.getAreas()));
                CellTree tree = new CellTree(model, null);

                VerticalPanel menu = new VerticalPanel();
                menu.setBorderWidth(1);
                menu.setWidth("300");
                menu.add(tree);

                VerticalPanel description = new VerticalPanel();
                description.setWidth("600");
                description.addStyleName("TestStyle");
                RootPanel.get().add(description);
                description.add(new Label(new Room("Per").getName()));

                HorizontalPanel mainPanel = new HorizontalPanel();
                mainPanel.setBorderWidth(2);
                mainPanel.add(menu);
                mainPanel.add(description);
                */

    //            RootPanel.get().add(t);

                mainPanel = new HorizontalPanel();
                VerticalPanel treeMenuPanel = new VerticalPanel();
                VerticalPanel descriptionPanel = new VerticalPanel();

                treeMenuPanel.setStyleName("treeStyle");

                treeMenuPanel.add(t);

                mainPanel.add(treeMenuPanel);
                mainPanel.add(descriptionPanel);

                RootPanel.get().add(mainPanel);
            }
        });



    }
}
